#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{

FILE *h;
    h = fopen("homelistings.csv", "r");
    if (h == NULL)
    {
        printf("Can't open homelistings.csv file\n");
        exit(1);
    }
    

    char address[40];
    int zip, id, price, beds, baths, sqft;
    while (fscanf(h, "%d, %d, %[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &beds, &baths, &sqft) != EOF)
    {
    
    FILE *zips;
    char filename[20];
    sprintf(filename, "%d.txt", zip);
    zips = fopen(filename, "a");
    
    if (zips == NULL)
    {
        printf("Can't open %s\n", filename);
        exit(1);
    }
    fprintf(zips, "%s\n", address);
    fclose(zips);
    }
    
}