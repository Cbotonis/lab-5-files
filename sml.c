#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *h;
    h = fopen("homelistings.csv", "r");
    if (h == NULL)
    {
        printf("Can't open homelistings.csv file\n");
        exit(1);
    }
    
    FILE *med;
    med = fopen("med.txt", "w");
    if (!med)
    {
        printf("Can't open med.txt\n");
        exit(1);
    }
    
    FILE *small;
    small = fopen("small.txt", "w");
    if (!small)
    {
        printf("Can't open small.txt\n");
        exit(1);
    }
    
    FILE *large;
    large = fopen("large.txt", "w");
    if (!large)
    {
        printf("Can't open large.txt\n");
        exit(1);
    }
    
    char address[40];
    int zip, id, price, beds, baths, sqft;
    int most_expensive = 0;
    int cheapest = 100000000;
    char money_bags[40];
    double total = 0;
    int counter = 0;
    double avg = 0;
    while (fscanf(h, "%d, %d, %[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &beds, &baths, &sqft) != EOF)
    {
        if (sqft < 1000)
        {
            fprintf(small, "%s : %d\n", address, sqft);
        }
        else if (sqft >= 1000 && sqft <2000)
        {
            fprintf(med, "%s : %d\n", address, sqft);
        }
        else
        {
            fprintf(large, "%s : %d\n", address, sqft);
        }
    }
    
}