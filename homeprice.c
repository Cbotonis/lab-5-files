#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *h;
    h = fopen("homes200.csv", "r");
    if (h == NULL)
    {
        printf("Can't open homes200.csv file\n");
        exit(1);
    }
    //95621,80023034,7613 Sara Lynn Way,187900,3,2,1431
    char address[40];
    int zip, id, price, beds, baths, sqft;
    int most_expensive = 0;
    int cheapest = 100000000;
    char money_bags[40];
    double total = 0;
    int counter = 0;
    double avg = 0;
    while (fscanf(h, "%d, %d, %[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &beds, &baths, &sqft) != EOF)
    {
        //printf("%s, %s %d %d\n", capital, name, pop, area);
        
        if (price > most_expensive)
        {
            most_expensive = price;
            strcpy(money_bags, address);
        }
        if (price < cheapest)
        {
            cheapest = price;
        }
        
        total += price;
        counter++;
        avg = total / counter;
    
    }
    fclose(h);
     printf("%d %d %.0lf\n", cheapest, most_expensive, avg);
}
    